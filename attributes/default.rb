# defaults for the recipes in this cookbook
default['devop']['chef_environment'] = '_default'
default['devop']['image_id'] = 'ami-f8f715cb'
default['devop']['instance_type'] = 't2.micro'
default['devop']['app_pool_name'] = 'DevopPool'
default['devop']['web_site_folder'] = 'C:\inetpub\sites\devop'
default['devop']['web_site_name'] = 'DevOp'
default['devop']['web_site_port'] = 80
default['devop']['website_h1_text'] = 'Automation for the People'

# for default.rb (via AWS environment variables)
default['devop']['region'] = "#{ENV['DEFAULT_REGION']}"
default['devop']['region_with_az'] = "#{ENV['DEFAULT_REGION']}#{ENV['DEFAULT_AZ']}"
default['devop']['key_name'] = "#{ENV['AWS_SSH_KEY_ID']}"
default['devop']['ingress_sources'] = ["#{ENV['INGRESS_SOURCE']}"]
# for infrastructure.rb (via CHEF environment values)
default['devop']['chef_validation_key'] = ["#{ENV['CHEF_ORG_VALIDATION_KEY']}"]
default['devop']['chef_signing_key'] = ["#{ENV['CHEF_USER_VALIDATION_KEY']}"]
default['devop']['chef_client_name'] = ["#{ENV['CHEF_CLIENT_NAME']}"]
