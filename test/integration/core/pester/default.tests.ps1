describe "devop::default" {

  it 'includes recipe <iis::default>' {
    # evidence: the 'IIS-WebServerRole' is installed
    $feature = Get-WindowsFeature -Name "Web-Server" | Where { $_.FeatureType -eq "Role" }
    $feature | Should Not BeNullOrEmpty
    $feature.Installed | Should Be $True
  }

  it 'includes recipe <iis::remove_default_site>' {
    # evidence: the 'Default' website does not exist
    $site = Get-Website -Name "Default Web Site"
    $ste | Should BeNullOrEmpty
  }

  it 'creates the index.html file from a template' {
    # evidence: 'index.html' exists at 'C:\inetpub\sites\devop'
    $folder = Get-Item -Path "C:/inetpub/sites/devop" | Where { $_.Attributes -eq "Directory" }
    $folder | Should Not BeNullOrEmpty

    $file = Get-Item -Path "C:/inetpub/sites/devop/index.html"
    $file | Should Not BeNullOrEmpty
    $content = Get-Content $file
    $target = $content | Where { $_.Contains("Automation for the People") }
    $target | Should Not BeNullOrEmpty
  }

  it 'creates an IIS application pool' {
    # evidence: presence of the 'DevopPool' iis application pool
    $pool = Get-ChildItem IIS:\apppools | Where { $_.Name -eq "DevopPool" }
    $pool | Should Not BeNullOrEmpty
    $pool.State | Should Be "Started"
  }

  it 'creates an IIS website' {
    # evidence: presence of the 'DevOp' iis site
    $site = Get-ChildItem IIS:\sites | Where { $_.Name -eq "DevOp" }
    $site | Should Not BeNullOrEmpty
    $site.State | Should Be "Started"
    $site.ApplicationPool | Should Be "DevopPool"
  }

}
