# prep the server with required features
%w{ IIS-WebServerRole IIS-WebServer NetFx4Extended-ASPNET45 IIS-HttpCompressionDynamic IIS-WebServerManagementTools IIS-ManagementConsole IIS-ApplicationDevelopment IIS-ApplicationInit IIS-ISAPIFilter IIS-ISAPIExtensions IIS-NetFxExtensibility45 IIS-ASPNET45 IIS-ManagementScriptingTools IIS-HttpRedirect }.each do |feature|
  windows_feature feature do
    action :install
  end
end

# ensures that IIS is installed and ready
include_recipe 'iis::default'

# kills the default web site
include_recipe 'iis::remove_default_site'

# defines where the website code will live
directory node['devop']['web_site_folder'] do
  recursive true
  #rights :read, ['IIS_IUSRS']
end

# add the index.html file
template "#{node['devop']['web_site_folder']}\\index.html" do
  inherits true
  source 'index.html.erb'
  variables({
    :website_h1_text => node['devop']['website_h1_text']
  })
end

# creates an app pool
iis_pool node['devop']['app_pool_name'] do
  runtime_version '4.0'

  action [:add,:start]
end

# creates a site in the new app pool
iis_site node['devop']['web_site_name'] do
  application_pool node['devop']['app_pool_name']
  path node['devop']['web_site_folder']
  port node['devop']['web_site_port']
  protocol :http

  action [:add, :start]
end
