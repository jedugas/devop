# Registers chef-client as a service
powershell_script 'register-chef-client-service' do
	cwd Chef::Config[:file_cache_path]
	guard_interpreter :powershell_script
	environment ({
		'svc_name' => 'chef-client',
		'expected_state' => 'DELAYED'
	})
	code <<-EOH
		Start-Process "C:/opscode/chef/bin/chef-service-manager" -ArgumentList "-a install" -Wait
		$service = Get-Service | ? { $_.Name -eq $env:svc_name }
		Stop-Service -Name $service.Name
		sc.exe config $service.Name start= delayed-auto
		Start-Service -Name $service.Name
	EOH
	not_if '$val = sc.exe qc $env:svc_name; $val.Count -gt 4 -and $val[4].Contains($env:expected_state)', :environment => environment
end
