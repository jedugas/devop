# This recipe will create the entire AWS resource stack necessary to provision a server
require 'chef/provisioning'
require 'chef/provisioning/aws_driver'

Chef::Config[:validation_key] = node['devop']['chef_validation_key']

region_with_az = node['devop']['region_with_az']
with_driver "aws::#{node['devop']['region']}"
with_chef_server "https://api.opscode.com/organizations/dugas",
  :client_name => node['devop']['chef_client_name'],
  :signing_key_filename => node['devop']['chef_signing_key']

# create a vpc specifically for this project
aws_vpc 'vpc_devop' do
  cidr_block '10.0.0.0/16'
  main_routes '0.0.0.0/0' => :internet_gateway
  internet_gateway true
  aws_tags 'Name' => 'vpc_devop'
end

# create a security group specifically for this project; disallow RDP and WinRM from anywhere but specifically-described ingress CIDRs
aws_security_group 'sg_devop' do
  inbound_rules [
    { :port => 80..80, :protocol => :tcp, :sources => [ "0.0.0.0/0" ] },
    { :port => 443..443, :protocol => :tcp, :sources => [ "0.0.0.0/0" ] },
    { :port => 5985..5986, :protocol => :tcp, :sources => node['devop']['ingress_sources'] },
    { :port => 3389..3389, :protocol => :tcp, :sources => node['devop']['ingress_sources'] }
  ]
  vpc 'vpc_devop'
end

aws_subnet 'sn_devop' do
  availability_zone region_with_az
  cidr_block '10.0.0.0/24'
  map_public_ip_on_launch true
  vpc 'vpc_devop'
end

# NOTE: TestKitchen should be used to create the actual machine, but this is how I would do it in code
# user_data = <<-SCRIPT
# <powershell>
# winrm quickconfig -q
# winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="300"}'
# winrm set winrm/config '@{MaxTimeoutms="1800000"}'
# winrm set winrm/config/service '@{AllowUnencrypted="true"}'
# winrm set winrm/config/service/auth '@{Basic="true"}'
#
# netsh advfirewall firewall add rule name="WinRM 5985" protocol=TCP dir=in localport=5985 action=allow
# netsh advfirewall firewall add rule name="WinRM 5986" protocol=TCP dir=in localport=5986 action=allow
#
# net stop winrm
# sc config winrm start=auto
# net start winrm
# </powershell>
# SCRIPT
#
# machine 'webserver' do
# 	recipe 'devop::register-client'
# 	ohai_hints 'ec2' => { 'a' => 'b' }
# 	chef_environment node['devop']['chef_environment']
# 	machine_options({ :is_windows => true,
# 		bootstrap_options: {
#       block_device_mappings: [
# 				{ :device_name => '/dev/sda1', :ebs => { volume_type: 'gp2', volume_size: 30, delete_on_termination: true } }
# 			],
# 			image_id: node['devop']['image_id'],
# 			instance_type: node['devop']['instance_type'],
# 			key_name: node['devop']['key_name'],
#       security_group_ids: ['sg_devop'],
#       subnet_id: 'sn_devop',
#       user_data: user_data
# 		}
#   })
#
#   action :converge
# end
#
# machine 'webserver' do
#   tags :windows, :webserver
#   recipe 'devop::default'
#
#   action :allocate
# end
