# devop

This repository is a Chef cookbook containing:

1. configuration for launching a Windows 2012 R2 server into an existing VPC, subnet and security group
2. a recipe converting it into a simple web server containing a static html website
3. reasonably complete suite of tests to demonstrate unit testing via ChefSpec and RSpec, as well as integration testing via Pester, a PowerShell-based testing suite.

This process is carried out by Test Kitchen and is self-contained provided you have the latest ChefDK installed. It does require you to define several environment variables (in order to keep sensitive values out of public repositories).

### Solution notes

For the sake of simplicity and support for unit and integration testing, I chose the combination of Chef and Test Kitchen. Via the 'ec2' driver, Test Kitchen enables the creation of a * development-scale, disposable server resource* which can run any Chef recipe, including recipes in this cookbook.

However, to demonstrate how to provision the supporting AWS infrastructure for a more complete infrastructure-as-code solution, see the 'infrastructure' recipe. It creates a VPC, a security group, and a subnet into which you can launch an instance (some instance code is there as well, just commented out since we're using Test Kitchen).

AWS infrastructure creation via chef-provisioning-aws _does_ tie you to Chef, but it has some distinct practical advantages over CloudFormation:

1. readability and terseness of code (consevatively about 1/3rd the 'code')
2. full-fledged unit testability
  *  you're going to want it when you see how gnarly a CF template can be, lol!
3. built-in resource referencing
  * Q. Just created a security group but needs its ID for the next resource/recipe?
  * A. No problem, chef-provisioning-aws look that up for you when we need it again.

If you would like to prove this out to yourself, create a Hosted Chef account (free!), configure a few more ENV variables (refer to the attributes to determine what you need to configure), and fire away:

    chef-client -z ./recipes/infrastructure.rb

### OK, let's run Test Kitchen

The bash script at the bottom of this README will prep a shell with all other pre-requisites - provide values specific to your 'developer' AWS account. A region-appropriate keypair (PEM) file must be in your ~/.ssh folder. AWS config and credentials files should be in your ~/.aws folder.

When it's done, clip the public IP out of the console output and go to http://{that_ip_you_clipped}. Be sure to destroy that instance when you're done (it's costing you money):

    kitchen destroy

<hr />

```bash
#!/bin/bash
#Change the variables below, make the file executable (chmod +x ./RUNME.sh), then run it (./RUNME.sh)
#############
#start
#############
# temporary ENV variables
export AWS_SSH_KEY_ID="AWS keypair name in that region"
export AWS_SUBNET_ID="public-facing VPC subnet ID in that region"
export AWS_SECURITY_GROUP_ID="security group with TCP ingress on ports 80 and 5985 (WinRM) for your computer's IP only"
export SHARED_CREDENTIALS_PROFILE="region-appropriate profile from ~/.aws/config and ~/.aws/credentials"
export DEFAULT_REGION="your region of choice, ex. us-west-2"
export DEFAULT_AZ="an availability zone covered by your subnet, ex. b"
export INGRESS_SOURCE="your computer's IP address (Google 'whatsmyip')"

# unit testing
chef exec rspec spec -c

# fulfill gem and cookbook requirements
chef exec gem install kitchen-ec2
chef exec gem install kitchen-pester
berks install

# provision the server, execute the recipe(s), integration testing
kitchen create
kitchen converge
kitchen verify
#############
#fin
#############
```
