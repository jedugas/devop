require 'spec_helper'

describe 'devop::default' do

  context 'When all attributes are default, on an unspecified platform' do

    let(:chef_run) {
      ChefSpec::SoloRunner.converge(described_recipe)
    }

    let(:html_content) {
<<-EOF
<!DOCTYPE html>
<html>
  <head>
    <title>DevOp</title>
  </head>
  <body>
    <h1>Automation for the People</h1>
  </body>
</html>
EOF
    }

    it 'converges successfully' do
      chef_run # This should not raise an error
    end

    it 'includes recipe <iis::default>' do
      expect(chef_run).to include_recipe("iis::default")
    end

    it 'includes recipe <iis::remove_default_site>' do
      expect(chef_run).to include_recipe("iis::remove_default_site")
    end

    it 'creates the index.html file from a template' do
      expect(chef_run).to create_template('C:\inetpub\sites\devop\index.html')
      expect(chef_run).to render_file('C:\inetpub\sites\devop\index.html').with_content(html_content)
    end

    it 'creates an IIS application pool' do
      expect(chef_run).to add_iis_pool('DevopPool')
    end

    it 'creates an IIS website' do
      expect(chef_run).to add_iis_site('DevOp')
    end

  end
end
